version: "3.3"

services:

  minio:
    container_name: minio
    hostname: minio
    image: minio/minio:RELEASE.2022-05-08T23-50-31Z
    command: server --console-address ":9001" /data/
    expose:
      - "9000"
      - "9001"
    networks:
      - s3
    environment:
      MINIO_ACCESS_KEY: $AWS_ACCESS_KEY_ID
      MINIO_SECRET_KEY: $AWS_SECRET_ACCESS_KEY
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3
    volumes:
      - ./Docker/minio/:/data

  nginx:
    container_name: nginx
    hostname: nginx
    image: nginx:1.19.2-alpine
    ports:
      - "9000:9000"
      - "9001:9001"
    networks:
      - s3
    volumes:
      - ./Docker/nginx.conf:/etc/nginx/nginx.conf:ro
    depends_on:
      - minio

  db:
    container_name: postgres
    hostname: postgres
    image: postgres
    restart: always
    environment:
      POSTGRES_USER: $POSTGRES_USER
      POSTGRES_PASSWORD: $POSTGRES_PASSWORD
      POSTGRES_DB: $POSTGRES_DB
      PGDATA: /data/postgres
    ports:
      - "5432:5432"
    networks:
      - postgres
    volumes:
      - postgres:/data/postgres

  pgadmin:
    container_name: pgadmin
    hostname: pgadmin
    image: dpage/pgadmin4
    restart: always
    environment:
      PGADMIN_DEFAULT_EMAIL: admin@admin.com
      PGADMIN_DEFAULT_PASSWORD: root
    ports:
      - "5050:80"
    networks:
      - postgres
    volumes:
      - ./pgadmin/:/var/lib/pgadmin

  mlflow:
    container_name: mlflow
    hostname: mlflow
    image: mlflow_server
    restart: always
    ports:
      - "5000:5000"
    networks:
      - s3
      - postgres
    environment:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      MLFLOW_S3_ENDPOINT_URL: http://nginx:9000
    command: mlflow server --backend-store-uri postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@db/$POSTGRES_DB --default-artifact-root s3://$AWS_S3_BUCKET/ --host 0.0.0.0

  app:
    container_name: model_service
    image: model_service
    ports:
      - "8003:80"
    networks:
      - s3
    environment:
      - MLFLOW_TRACKING_URI=http://mlflow:5000
      - MLFLOW_S3_ENDPOINT_URL=http://nginx:9000
    volumes:
      - ./data/interim/full_dataset.csv:/data/full_dataset.csv
      - ./models/price_scaler.pkl:/data/price_scaler.pkl

networks:
  s3:
    driver: bridge
  postgres:
    driver: bridge

volumes:
  postgres: