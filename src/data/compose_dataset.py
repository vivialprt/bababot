import click
import pandas as pd


def compose_dataset(
    sales, shop_df, item_df, cat_df
):
    return (
        sales
        .merge(shop_df, on='shop_id', how='left')
        .merge(item_df, on='item_id', how='left')
        .merge(cat_df, on='item_category_id', how='left')
    )


@click.command
@click.argument('input_filenames', type=click.Path(exists=True), nargs=4)
@click.argument('output_filename', type=click.Path())
def compose_dataset_wrapper(input_filenames, output_filename):

    # Import data
    sales = pd.read_csv(input_filenames[0])
    shop_df = pd.read_csv(input_filenames[1])
    item_df = pd.read_csv(input_filenames[2])
    cat_df = pd.read_csv(input_filenames[3])

    dataset = compose_dataset(sales, shop_df, item_df, cat_df)

    # Save data
    dataset.to_csv(output_filename, index=False)


if __name__ == '__main__':
    compose_dataset_wrapper()
