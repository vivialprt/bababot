from src.features.scaling import scale_data
from src.features.aggregating import aggregate
from src.models.utils import get_matrix, get_windows
from src.models.train_model import WINDOW_SIZE
import pickle


def preprocess(df, scaler_path='/data/price_scaler.pkl'):

    with open(scaler_path, 'rb') as f:
        scaler = pickle.load(f)

    df = aggregate(df)
    df, _ = scale_data(df, scaler)
    mx, index = get_matrix(df)
    _, _, window = get_windows(mx, WINDOW_SIZE)
    return window[:, 1:], index
