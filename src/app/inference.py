import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException
import sys
sys.path.append(os.path.abspath('..'))

from app.data_retrive import DataRetriever
from app.preprocess import preprocess

load_dotenv()

app = FastAPI()


class Model:
    def __init__(self, model_name, model_stage) -> None:
        self.model = mlflow.pyfunc.load_model(f'models:/{model_name}/{model_stage}')

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions[:, 0]


model = Model('sales_lstm', 'staging')
data_retriever = DataRetriever()


@app.post('/invocations')
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith('.csv'):
        with open(file.filename, 'wb') as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)
        df = data_retriever.get_data_by_shop_and_item(data)
        mx, index = preprocess(df)
        preds = model.predict(mx).tolist()
        index['prediction'] = pd.Series(preds)
        return index.to_dict(orient='records')

    else:
        raise HTTPException(status_code=404, detail='Invalid file format')


if os.getenv('AWS_ACCESS_KEY_ID') is None or os.getenv('AWS_SECRET_ACCESS_KEY') is None:
    exit(1)
