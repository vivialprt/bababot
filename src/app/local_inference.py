import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
import sys
sys.path.append(os.path.abspath('../..'))

from data_retrive import DataRetriever
from preprocess import preprocess

load_dotenv()


class Model:
    def __init__(self, model_name, model_stage) -> None:
        self.model = mlflow.pyfunc.load_model(f'models:/{model_name}/{model_stage}')

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions[:, 0]


if os.getenv('AWS_ACCESS_KEY_ID') is None or os.getenv('AWS_SECRET_ACCESS_KEY') is None:
    exit(1)


if __name__ == '__main__':
    model = Model('sales_lstm', 'staging')
    data_retriever = DataRetriever('../../data/interim/full_dataset.csv')
    data = pd.read_csv('../../data/raw/inference.csv')
    df = data_retriever.get_data_by_shop_and_item(data)
    mx, index = preprocess(df, '../../models/price_scaler.pkl')
    preds = model.predict(mx).tolist()
    index['prediction'] = pd.Series(preds)
    print(index)
