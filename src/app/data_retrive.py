import pandas as pd


class DataRetriever:

    def __init__(self, full_df_path='/data/full_dataset.csv'):
        self.full_df = pd.read_csv(full_df_path)

    def get_data_by_shop_and_item(self, shop_and_item_df):
        df = shop_and_item_df[['shop_id', 'item_id']]
        df = df.merge(
            self.full_df,
            on=['shop_id', 'item_id'],
            how='left', indicator=True
        )

        pairs_with_no_data = df.loc[df._merge == 'left_only']
        shops_to_avg = pairs_with_no_data.shop_id.unique()
        df.drop(['_merge'], axis=1, inplace=True)

        shops_avg = (
            self.full_df.loc[self.full_df.shop_id.isin(shops_to_avg)]
            .groupby(['date_block_num', 'shop_id'])
            .agg({
                'item_price': 'mean',
                'item_cnt_day': 'mean'
            })
            .reset_index()
        )

        isna_df = df.loc[
            df.date_block_num.isna(),
            ['item_id', 'shop_id']
        ]
        df = df.loc[df.date_block_num.notna()]

        isna_df['date_block_num'] = isna_df.apply(lambda _: list(range(34)), axis=1)
        isna_df = isna_df.explode('date_block_num')
        isna_df = isna_df.merge(
            shops_avg,
            on=['date_block_num', 'shop_id'],
            how='inner'
        )

        df = df[isna_df.columns]
        df = pd.concat([df, isna_df], ignore_index=True)

        return df
