import click
import pandas as pd


CLIP_BOUND_LOW = 0.
CLIP_BOUND_HIGH = 20.


def aggregate(df):

    df = df.groupby([
        'date_block_num',
        'item_id',
        'shop_id'
    ]).agg({'item_cnt_day': 'sum', 'item_price': 'mean'}).reset_index()
    df = df[['date_block_num', 'item_id', 'shop_id', 'item_cnt_day', 'item_price']]
    df['item_cnt_day'].clip(CLIP_BOUND_LOW, CLIP_BOUND_HIGH, inplace=True)
    return df


@click.command
@click.argument('input_filename', type=click.Path(exists=True))
@click.argument('output_filename', type=click.Path())
def aggregate_wrapper(
    input_filename, output_filename,
):
    df = pd.read_csv(input_filename)

    df = aggregate(df)

    df.to_csv(output_filename, index=False)


if __name__ == '__main__':
    aggregate_wrapper()
