import click
import pandas as pd
import pickle
from sklearn.preprocessing import MinMaxScaler


def scale_data(df, prices_scaler=None):

    if not prices_scaler:
        prices_scaler = MinMaxScaler()
        df['item_price'] = prices_scaler.fit_transform(
            df['item_price'].values.reshape(-1, 1)
        )
    else:
        df['item_price'] = prices_scaler.transform(
            df['item_price'].values.reshape(-1, 1)
        )

    return df, prices_scaler


@click.command
@click.argument('input_filename', type=click.Path(exists=True))
@click.argument('output_filename', type=click.Path())
@click.argument('price_scaler_filepath', type=click.Path())
def scale_data_wrapper(
    input_filename, output_filename,
    price_scaler_filepath
):
    df = pd.read_csv(input_filename)

    df, scaler = scale_data(df)

    df.to_csv(output_filename, index=False)

    with open(price_scaler_filepath, 'wb') as f:
        pickle.dump(scaler, f)


if __name__ == '__main__':
    scale_data_wrapper()
