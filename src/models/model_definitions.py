import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

from keras.models import Sequential
from keras.layers import LSTM, Dense, Dropout, Input
from keras.metrics import RootMeanSquaredError


def build_lstm_model(
    input_shape,
    lstm_units=(16, 32),
    dropout_rates=(.5, .5),
    optimizer='adam',
    loss='mse',
    metrics=(RootMeanSquaredError(),)
):
    model = Sequential()
    model.add(Input(shape=input_shape))
    for n_lstm, dropout_rate in zip(lstm_units, dropout_rates):
        model.add(LSTM(n_lstm, return_sequences=True))
        model.add(Dropout(dropout_rate))

    model.layers[-2].return_sequences = False
    model.add(Dense(1))
    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
    return model
