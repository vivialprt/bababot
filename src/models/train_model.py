from src.models.model_definitions import build_lstm_model
from src.models.utils import get_matrix, get_windows
import click
import mlflow
import numpy as np
import pandas as pd
from sklearn.metrics import mean_squared_error
from keras.callbacks import EarlyStopping
from dotenv import load_dotenv
import os

load_dotenv()


TRAIN_PARAM = {
    "batch_size": 128,
    "verbose": 2,
    "epochs": 1
}
SEED = 2023
WINDOW_SIZE = 28


mlflow.set_experiment('LSTM')
mlflow.set_tracking_uri(os.getenv('MLFLOW_TRACKING_URI'))
mlflow.get_artifact_uri()


def train(df, model_path):

    df = df[['date_block_num', 'item_id', 'shop_id', 'item_price', 'item_cnt_day']]
    train_matrix, _ = get_matrix(df)
    train_windows, valid_window, test_window = get_windows(train_matrix, WINDOW_SIZE)

    X_train, y_train = train_windows[:, :-1], train_windows[:, -1, 0]
    X_valid, y_valid = valid_window[:, :-1], valid_window[:, -1, 0]
    X_test, y_test = test_window[:, :-1], test_window[:, -1, 0]

    print(f"Train Shape: X: {X_train.shape} y: {y_train.shape}")
    print(f"Valid Shape: X: {X_valid.shape} y: {y_valid.shape}")
    print(f"Test Shape: X: {X_test.shape} y: {y_test.shape}")

    callbacks_list = [EarlyStopping(
        monitor="val_loss", min_delta=.0001, patience=3, restore_best_weights=True
    )]
    model = build_lstm_model(X_train.shape[1:], lstm_units=[16, 8])
    mlflow.log_params(TRAIN_PARAM)

    hist = model.fit(
        X_train, y_train,
        validation_data=(X_valid, y_valid),
        callbacks=callbacks_list,
        **TRAIN_PARAM
    )

    y_pred = model.predict(X_test)
    mse = mean_squared_error(y_test, y_pred)
    scores = {
        'test_mse': mse,
        'test_rmse': mse ** .5
    }

    best = np.argmin(hist.history['val_loss'])
    print(f'Optimal Epoch: {best}')
    print(f'Train Score: {hist.history["root_mean_squared_error"][best]}')
    print(f'Validation Score: {hist.history["val_root_mean_squared_error"][best]}')
    print(f'Test Score: {mse ** .5}')

    mlflow.log_metric('train_mse', hist.history["loss"][best])
    mlflow.log_metric('train_rmse', hist.history["root_mean_squared_error"][best])
    mlflow.log_metric('val_mse', hist.history["val_loss"][best])
    mlflow.log_metric('val_rmse', hist.history["val_root_mean_squared_error"][best])
    mlflow.log_metrics(scores)

    signature = mlflow.models.infer_signature(X_test, y_pred)
    mlflow.keras.log_model(
        keras_model=model, artifact_path=model_path,
        registered_model_name='sales_lstm', signature=signature
    )
    return model


@click.command
@click.argument('train_input', type=click.Path(exists=True))
@click.argument('model_path', type=click.Path())
def train_wrapper(train_input, model_path):
    df = pd.read_csv(train_input)
    model = train(df, model_path)
    model.save(model_path)


if __name__ == '__main__':
    train_wrapper()
