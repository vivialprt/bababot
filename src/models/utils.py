import numpy as np


def get_matrix(df, index_col=None):
    if index_col is None:
        index_col = ['item_id', 'shop_id']

    sales_df = df.pivot_table(
        index=index_col,
        columns='date_block_num',
        values='item_cnt_day',
        fill_value=0
    )

    price_df = df.pivot_table(
        index=index_col,
        columns='date_block_num',
        values='item_price',
        fill_value=0
    )

    sales_mx = sales_df.values
    price_mx = price_df.values

    return (
        np.dstack([sales_mx, price_mx]),
        sales_df.index.to_frame().reset_index(drop=True)
    )


def get_windows(mx, wsize):
    max_seq_len = mx.shape[1]
    windows = [mx[:, i: i + wsize + 1] for i in range(max_seq_len - wsize)]

    train_windows = np.vstack(windows[:-2])
    valid_window = windows[-2]
    test_window = windows[-1]

    return train_windows, valid_window, test_window
